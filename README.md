# btw-st
My build of Simple (Suckless) Terminal

## Patches

- Anysize - st can be anysize fixing weird gaps with st and other windows
- Bold is not bright - Bold will not be bright unlike default behavior
- Boxdraw - Custom rendering of lines/blocks/braille characters for gapless alignment
- Dynamic Cursor Color - What it says
- Ligatures - Add ligature support
- Tokyo Night Storm - Color scheme

##### The bindings are default aside from the ones listed above

## Help/FAQ

- What version of st is this?
This build is currently on version 0.8.4

## Installation Instructions

```
git clone https://gitlab.com/ArchB1W/st
cd st
sudo make clean install
```

Only build requirements are `make`, `libx11-dev`, `libxft-dev`, and `harfbuzz-dev` for ligatures
